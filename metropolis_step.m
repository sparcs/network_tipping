function S1 = metropolis_step(S, M, J, h, beta, fixedindex, iterfract, hfixed)
    %two ways of performing efficient step in metropolis algorithm
    %     if false
    %         %vectorized, but slower for niter=100 (MATLAB 2020)
    %         sum_Sj = J .* M * S;
    %         % Calculate the change in energy of flipping a spin of all cells
    %         DeltaH = 2 .* (S .* sum_Sj) + 2 .* h .* S;
    %         % Calculate the transition probabilities
    %         p_trans = exp(-beta * DeltaH);
    %         % Decide which transitions will occur
    %         transitions = (rand(size(S)) < p_trans ) .* (rand(size(S)) < iterfract) * -2 + 1;
    %         % Perform the transitions
    %         S1 = S .* transitions;
    %     else
%     if nargin<8
%         hfixed=Inf;
%     end
    niter = round(numel(S) * iterfract);
    S1 = S;
    sumneighbors = M * S; %should be done every step, but doing it once saves much speed
    ndx = randi(numel(S), niter, 1); %pick niter random spins (small gain)
    randp = rand(niter, 1); %pick niter rand values (small gain)
    for iter = 1 : niter
        % Pick a random spin
        linearIndex = ndx(iter);
        % Calculate energy change if this spin is flipped
        if linearIndex==fixedindex
           DeltaH = 2 * J * S(linearIndex) * sumneighbors(linearIndex) + 2 * hfixed * S(linearIndex);
        else
           DeltaH = 2 * J * S(linearIndex) * sumneighbors(linearIndex) + 2 * h * S(linearIndex);
        end
        % Boltzmann probability of flipping
        %prob = exp(-dE * beta); (in the if statement is more efficient)
        % Spin flip condition
        if DeltaH <= 0 || randp(iter) <= exp(-DeltaH * beta)
            S1(linearIndex) = -S(linearIndex);
        end
    end
%     if fixedindex > 0
%         S1(fixedindex) = S(fixedindex);
%     end
end
