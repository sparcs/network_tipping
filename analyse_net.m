function res = analyse_net(m1, d1, M1, newm)
    global m N d g_Y M C r K;
    if nargin >= 1
        m = m1;
    end
    if nargin >= 2
        d = d1;
    end
    if nargin >= 3
        M = M1;
    end
    if nargin < 4
        newm = NaN;
    end
    equilN = @(m)(C*r + K*r + (C^2*r^2 - 2*C*K*r^2 - 4*m*C*K*r + K^2*r^2)^(1/2))/(2*r);
    %saddleN=@(m)(C*r + K*r - (C^2*r^2 - 2*C*K*r^2 - 4*m*C*K*r + K^2*r^2)^(1/2))/(2*r);
    res.m = m;
    res.C = C;
    res.d = d;
    res.r = r;
    res.K = K;
    res.newm = newm;
    if ~isnan(newm)
        m0 = zeros(size(N)) + res.m;
    end
    res.nsteps = int16(400);
    simtime(0, double(res.nsteps), double(res.nsteps / 10));
    N0 = real(zeros(size(N)) + equilN(res.m));
    res.shifted = false(length(N), length(N));
    res.nrun = ones(size(N), 'int16');
    for i = 1:length(N)
        N = N0;
        N(i) = 0;
        if ~isnan(newm)
            m = m0;
            m(i) = newm;
        end
        time('-s');
        while max(abs(g_Y(end, :) - g_Y(end - 1, :))) > 1E-4 && res.nrun(i) < 10
            ke;
            res.nrun(i) = res.nrun(i) + 1;
            fprintf('Expand run-%d: %d times\n', i, res.nrun(i) - 1)
            time('-s');
        end
        res.shifted(i, :) = g_Y(end, :) < 0.5 * equilN(res.m);
    end
    if ~isnan(newm)
        m = res.m;
    end
end

