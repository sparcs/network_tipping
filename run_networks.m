function nwork = run_networks(dim, ntype, par, nrep, newm, ms, ds)
    if nargin < 3
        dim = 50;
    end
    if nargin < 3
        ntype = 's';
    end
    if nargin < 3
        par = 2.5;
    end
    if nargin < 4
        nrep = 1;
    end
    if nargin < 5
        newm = NaN; %the resilience of the node that is tested can also be changed, give the value for m in this parameter
        %NaN is unchanged
    end
    if nargin < 6
        if isnan(newm)
            ms = [1.95:0.002:2.03];
        else
            ms = [1.8:0.005:2.03];
        end
    end
    if nargin < 7
        if isnan(newm)
            ds = [0.05, 0.15];
        else
            ds = [0.05, 0.15, 0.4];
        end
    end
    if strcmp(ntype, 'r') && par > 1
        par = (dim * par * 2) / dim / dim; %theoretical maximum connectance for 'e' and 's' models with legs=par
    end
    nwork = struct('M', cell(1, nrep), 'closeness', 0, 'type', ntype, 'par', par, ...
        'res', [], 'newm', newm);
    resfile = 'nworks%d.mat';
    use allee_network
    setdimension('N', dim);
    fprintf('dim = %g\ntype=%s\npar= %g\nnrep=%d\nnewm=%g\n', dim, ntype, par, nrep, newm);

    ns = length(ms) * length(ds);
    for k = 1:nrep
        [nwork(k).M, nwork(k).closeness] = create_valid_network(dim, ntype, par);
        nwork(k).res = struct('m', cell(length(ms), length(ds)), 'C', 0, 'd', 0, 'r', 0, 'K', 0, 'newm', 0, 'nsteps', 0, 'shifted', 0, 'nrun', 0);
       for j = 1:length(ds)
           for i = 1:length(ms)
                tic;
                nwork(k).res(i, j) = analyse_net(ms(i), ds(j), nwork(k).M, newm);
                fprintf('rep %d run %d/%d: m=%g, d=%g; toc=%g\n', k, (i - 1) * length(ds) + j, ns, ms(i), ds(j), toc);
            end
        end
    end
    n0 = 1;
    while exist(sprintf(resfile, n0), 'file')
        n0 = n0 + 1;
    end
    save(sprintf(resfile, n0), 'nwork');
end

