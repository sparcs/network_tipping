%run_ising_networks property-value pairs:
%properties:
%  type ('e')
%  par (2.5)
%  ndays (4000)
%  newm (inf) or 0 (inf=keep node in other state, 0 = not);
%  niter for percshifted (100)
%  nnetworks iters for networks (100)
%  h value of parameter h (current)
%
%
function result = run_ising_networks(varargin)
    outfile = datestr(now(), 'run_dd_mmm_yyyy_HH_MM_SS');
    if nargin == 1
        if isstruct(varargin{1})
            args = varargin{1};
        elseif isa(varargin{1}, 'table')
            utab = varargin{1};
            for i = 1:size(utab, 1)
                if utab.n(i) > 0 && utab.ising(i)
                    %try
                        result = run_ising_networks('type', utab.type{i}, 'par', utab.par(i), 'newm', utab.newm(i), 'h', utab.h_or_d(i), 'nnetworks', utab.n(i));
                    %catch
                    %    result = run_ising_networks('type', utab.type{i}, 'par', utab.par(i), 'newm', utab.newm(i), 'h', utab.h(i), 'nnetworks', utab.n(i));
                    %end
                end
            end
            return;
        end
    else
        args = struct(varargin{:});
    end
    
    if ~isfield(args, 'type')
        args.type = 'e';
    end
    if ~isfield(args, 'par')
        if any(strcmp(args.type, {'e', 's'}))
            args.par = 2.5;
        elseif strcmp(args.type, 'r')
            args.par = 0.01; %should be 10x less than for a 50x50 model
        elseif strcmp(args.type, 'rr')
            args.par = 4;
        end
    end
    if ~isfield(args, 'ndays')
        args.ndays = 4000;
    end

    if ~isfield(args, 'niter')
        args.niter = 100;
    end
    if ~isfield(args, 'nnetworks')
        args.nnetworks = 100;
    end
    if ~isfield(args, 'newm')
        args.newm = [inf 0];
        %inf is keep to the other state
        %0 
    end
    if ~isfield(args, 'h')
        if strcmp(args.type, 'e') && args.par == 2.5
            args.h = 1.60;
        elseif strcmp(args.type, 's') && args.par == 2.5
            args.h = 1.2;
        elseif strcmp(args.type, 'r') && args.par == 0.01
            args.h = 1.3;
        elseif strcmp(args.type, 'rr') && args.par == 4
            args.h = 1;
        else
            args.h = 0;
        end
        %evalin('base', 'h');
    end
    fieldnams = {'type', 'par', 'ndays', 'niter', 'nnetworks', 'newm', 'h'};
    if any(~ismember(fieldnames(args), fieldnams))
        ndx = ~ismember(fieldnames(args), fieldnams);
        f = fieldnames(args);
        error('Unknown fieldname(s): %s', strjoin(f(ndx)))
    end
    args
    assignin('base', 'h', args.h);
    oldS = evalin('base', 'S');
    S = -ones(size(oldS));
    assignin('base', 'S', S);
    assignin('base', 'beta', 1)
    assignin('base', 'J', 1)
    result = struct('M', cell(args.nnetworks * numel(args.newm), 1), 'closeness', [], 'type', args.type, 'par', args.par, 'res', [], 'newm', args.newm(1), 'rundate', 0);
    k = 1;
    for i = 1:args.nnetworks
        disp('creating network')

        M = create_network(numel(S), args.type, args.par);
        assignin('base', 'M', M)
        closeness = close_centr(M);
        for m1 = 1:length(args.newm)
            result(k).M = M;
            result(k).closeness = closeness;
            result(k).rundate = now();
            result(k).newm = args.newm(m1);
            %result.type=args.type;
            %result.par=args.par;
            %result.res=[];
            %result.newm=NaN;
            res1 = par('-v');
            res = rmfield(res1, 'M');
            res.ndays = args.ndays;
            res.nrun = args.niter;
            res.shifted = false(length(S), args.niter);
            for j = 1:length(S)
                if args.newm(m1) > 0
                    assignin('base', 'fixedindex', j)
                    assignin('base', 'hfixed', args.newm(m1))
                else
                    assignin('base', 'fixedindex', -1);
                end
                S = -ones(size(S));
                S(j) = 1;
                assignin('base', 'S', S);
                res1 = run_parallel('tstep', 2, 'ndays', args.ndays);
                for k1 = 1:args.niter
                    %stabil(struct('keep', false, 'silent', true, 'ndays', args.ndays));
                    res.shifted(j, k1) = mean(res1.Y{k1}(end, :) > 0) > 0.5;
                end
                %             for k = 1:args.niter
                %                 stabil(struct('keep', false, 'silent', true, 'ndays', args.ndays));
                %                 res.shifted(j, k) = mean(g_Y(end, :)) > 0;
                %             end
                fprintf('%d-%g: node %d = %g  (closeness=%g)\n', i, args.newm(m1), j, sum(res.shifted(j, :)) / args.niter, closeness(j));
            end
            result(k).res = res;
            nworks_ising = result(1:k);
            k = k + 1;
            save(outfile, 'nworks_ising');
        end
    end
end
