function [CLOSENESS ShortstPthMAT TimesFoundMAT]=close_centr(M,bidirect)


%% find shortest routes with Dijkstra's algorithm
%% each link has length=1

%% input 'NET' can look as follows for a undirected unipartite network:
%% NET=matrix of zeros and ones
%translated to [1 2; 2 1; 1 3; 3 1; 1 4; 4 1; 2 3; 3 2; 3 4; 4 3; 4 5; 5 4; 4 6; 6 4; 5 6; 6 5; 5 7; 7 5; 5 8; 8 5; 6 7; 7 6; 6 8; 8 6; 7 8; 8 7; 7 9; 9 7]

%% the highest number assigned to a node, is assumed to be equal to the number of nodes in the network

%% Jelle Lever dd 19-04-2013

if nargin<2
    bidirect=true;
end;
%set the diagonal to 0
M(1:(size(M,1)+1):end)=0;

[ndx1,ndx2]=find(M>0);
if bidirect
   NET=[[ndx1;ndx2],[ndx2;ndx1]];
else
   NET=[ndx1,ndx2];
end;

NRnodes=max(max(NET));
for NRnodex=1:NRnodes
    
    %% display Node of which the shortest path is determined
    %disp(NRnodex);

    %% reset info on paths found
    LASTfound=NRnodex;
    ALLfoundLIST=NRnodex;
    PathLength=1;
    TIMESfound=zeros(NRnodes,1);
    
    %% within node distance is 0
    ShortstPthMAT(NRnodex,NRnodex)=0;
    TimesFoundMAT(NRnodex,NRnodex)=1;
    
    % search for shortest paths untill all shortest paths are found
    ALLfound=0;
    safetynr=0;
    while ALLfound~=(NRnodes-1)
        
        % go through all interactions
        LASTfoundNEW=[];
        for NRint=1:length(NET)
            
            % check if node in position '1' was found during the last while loop
            ONlastout=sum(LASTfound==NET(NRint,1));
            
            % check if node in position '2' was not found yet
            yOnALLout=sum(ALLfoundLIST==NET(NRint,2));
            
            NRnodey=NET(NRint,2);
            if ONlastout>=1 && yOnALLout==0;
                
                NRnodey=NET(NRint,2);
                LASTfoundNEW((length(LASTfoundNEW)+1),1)=NRnodey;
                TIMESfound(NRnodey,1)=TIMESfound(NRnodey,1)+TimesFoundMAT(NRnodex,(NET(NRint,1)));
                
                %update shortest pathlength and nr of shortest paths
                ShortstPthMAT(NRnodex,NRnodey)=PathLength;
                TimesFoundMAT(NRnodex,NRnodey)=TIMESfound(NRnodey,1);
            end
            
        end
        
        ALLfoundLIST([(length(ALLfoundLIST)+1):(length(ALLfoundLIST)+length(LASTfoundNEW))],1)=LASTfoundNEW;
        LASTfound=LASTfoundNEW;
        PathLength=PathLength+1;
        ALLfound=sum(TIMESfound>0);
        
        safetynr=safetynr+1;
        if safetynr==3000
            ALLfound=NRnodes-1;
            disp(['!!!!warning -- after many attempts no route found between two nodes!!!!']);
            CLOSENESS=[];
            return;
        end

    end
end

CLOSENESS=(NRnodes-1)./sum(ShortstPthMAT,2);