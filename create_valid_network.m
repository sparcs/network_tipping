function [M,closeness,degree]=create_valid_network(varargin)
closeness=[];
while isempty(closeness)||length(closeness)<varargin{1}
  M=create_network(varargin{:});
  closeness=close_centr(M);
end
degree=full(sum(M,2));
fprintf('connectance = %g\n',sum(sum((full(M))))/(numel(M)));
M(1:(size(M,1)+1):end)=-degree; %diffusion: set the diagonal to -sum;
