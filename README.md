# network_tipping


## Description
This project gives code to analyse two kinds of networks: each node has an ising model or each node has an Allee effect model. It needs MATLAB and GRIND for MATLAB. There are also some example movies given.


## Usage
To run the Allee effect model, type the following commands in MATLAB:

use allee_network\
nwork = run_networks(dim, ntype, par, nrep, newm, ms, ds)

arguments:

'dim' the dimension of the network\
'ntype', the type of network: 'e'=exponential, 's'=scale free, 'r'=random\
'par', the parameter for generating the network (dependent on the network, for instance for 'e', par= the number of legs).\
'nrep', the number of different networks to run
'newm', what is done with the disturbed node? Inf= the node is kept to the other state, 0=only the biomass is reduced\
'ms', a list of mortality values (m) to run\
'ds', a list of dispersion coefficients (d) to run

To run the Ising model, type the following commands in MATLAB:

use ising_network\
run_ising_networks('parameter',value,'parameter',value)

parameters:

'type', the type of network 'e'=exponential, 's'=scale free, 'r'=random\
'par', the parameter for generating the network (dependent on the network, for instance for 'e', par= the number of legs).\
'ndays', number of time units to run\
'niter', number of iterations per network\
'nnetworks', number of different networks to run\
'newm', what is done with the disturbed node? Inf= the node is kept to the other state, 0=only the biomass is reduced\
'h', the external magnetic field\

## Author
E. van Nes (egbert.vannes@wur.nl)\
